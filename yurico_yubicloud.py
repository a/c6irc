import secrets
import base64
import hmac
import httpx
from yurico_helpers import (
    yubicloud_servers,
    print_bold,
    re_yubicloud_otp,
    re_otp,
    yubicloud_reuse_responses,
    yubicloud_bad_responses,
    get_serial,
    fancy_dict_print,
)


def _calc_signature(text: str, api_secret: str) -> str:
    key = base64.b64decode(api_secret)
    signature_bytes = hmac.digest(key, text.encode(), "SHA1")
    return base64.b64encode(signature_bytes).decode()


def _validate_response_signature(response_dict: dict, api_secret: str) -> bool:
    yubico_signature = response_dict["h"]
    to_sign = ""
    for key in sorted(response_dict.keys()):
        if key == "h":
            continue
        to_sign += f"{key}={response_dict[key]}&"
    our_signature = _calc_signature(to_sign.strip("&"), api_secret)
    return our_signature == yubico_signature


def validate_otp(
    otp: str,
    servers: list = yubicloud_servers,
    api_client_id: int = 1,
    api_secret: str = None,
) -> dict:
    nonce = secrets.token_hex(15)  # Random number in the valid range
    params = f"id={api_client_id}&nonce={nonce}&otp={otp}"

    # If secret is supplied, sign our request
    if api_secret:
        params += "&h=" + _calc_signature(params, api_secret)

    for api_server in servers:
        url = f"{api_server}/wsapi/2.0/verify?{params}"
        try:
            resp = httpx.get(url)
            assert resp.status_code == 200
        except Exception as ex:
            print_bold(f"Got {repr(ex)} on {api_server}.", stderr=True)
            continue
        resptext = resp.text

        # Turn the fields to a python dict for easier parsing
        datafields = resptext.strip().split("\r\n")
        datafields = {
            line[: line.index("=")]: line[line.index("=") + 1 :] for line in datafields
        }

        # Verify nonce
        assert datafields["nonce"] == nonce

        # Verify signature if secret is present
        if api_secret and not _validate_response_signature(datafields, api_secret):
            return {
                "success": False,
                "reason": "Server response has invalid signature.",
            }

        # If we got a success, then return True
        if datafields["status"] == "OK":
            result = {"success": True}

            # Include serial on success
            serial = get_serial(otp)
            if serial:
                result["serial"] = serial

            return result
        elif datafields["status"] in yubicloud_reuse_responses:
            return {
                "success": False,
                "reason": f"OTP is invalid or reused: {datafields['status']}.",
            }

        # If status isn't an expected one, print it
        print_bold(
            f"Got {repr(datafields)} on {api_server} with and nonce {nonce}",
            stderr=True,
        )

        # If we fucked up in a way we can't recover from, just return None
        if datafields["status"] in yubicloud_bad_responses:
            return {
                "success": False,
                "reason": f"Server returned error: {datafields['status']}.",
            }

    # Return None if we fail to get responses from any server
    return {
        "success": False,
        "reason": "No servers responded",
    }


if __name__ == "__main__":
    import re
    import click
    import json

    @click.command()
    @click.argument("otp", envvar="YURICO_OTP")
    @click.option(
        "--servers",
        help='Verification server(s) to use, separated with ","',
        envvar="YURICO_VERIFICATION_SERVERS",
        default=",".join(yubicloud_servers),  # hack
    )
    @click.option(
        "--apiclientid",
        help="Client ID to use with verification server(s)",
        envvar="YURICO_VERIFICATION_CLIENT_ID",
        default=1,
    )
    @click.option(
        "--apiclientsecret",
        help="Client secret to use with verification server(s)",
        envvar="YURICO_VERIFICATION_CLIENT_SECRET",
    )
    @click.option(
        "--jsonoutput", help="JSON output", is_flag=True, envvar="YURICO_JSON"
    )
    def cli_verify_otp(
        otp,
        servers=yubicloud_servers,
        apiclientid=1,
        apiclientsecret=None,
        jsonoutput=False,
    ):
        servers = servers.split(",")
        is_yubicloud = servers == yubicloud_servers
        if not re.fullmatch(re_yubicloud_otp if is_yubicloud else re_otp, otp):
            print_bold("Error: OTP does not match the required format.", stderr=True)
            return

        validate_result = validate_otp(otp, servers, apiclientid, apiclientsecret)
        if jsonoutput:
            print(json.dumps(validate_result))
        else:
            fancy_dict_print(validate_result)

    cli_verify_otp()
