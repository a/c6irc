import pydle
import re
import json
import yurico_yubicloud
import yurico_helpers

with open("config.json") as f:
    config = json.load(f)


class c6(pydle.Client):
    yubico_re = re.compile(yurico_helpers.re_yubicloud_otp_anywhere)

    async def on_connect(self):
        print("Up and running. Joining all channels...")
        for channel in config["channels"].keys():
            print(f"Joining {channel}")
            await self.join(channel)
        print("Joined all channels...")

    async def on_message(self, target, source, message):
        if source == self.nickname:
            return

        otps = self.yubico_re.findall(message.strip())
        if otps:
            otp = otps[0][0].replace("'", "i")
            print(f"Trying to nuke {otp} by {source} in {target}")

            # Validate OTP
            # TODO: add async!
            validation_result = yurico_yubicloud.validate_otp(otp)
            if validation_result.get("success") is not True:
                return

            channel_config = config["channels"].get(target, {})

            # Derive serial and a string to use it
            serial = yurico_helpers.get_serial(otp)
            serial_str = f' (serial: "{serial}")' if serial else ""

            if channel_config.get("message_channel", False):
                await self.message(
                    target, f'{source}: Ate Yubico OTP "{otp}"{serial_str}'
                )
            if channel_config.get("message_user", False):
                await self.message(source, f'Ate Yubico OTP "{otp}"{serial_str}')


client = c6(**config["credentials"])
client.run(**config["server"])
