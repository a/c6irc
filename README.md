# c6 (IRC)

IRC bot to nuke accidentally posted Yubico OTPs.

The name comes from the fact that it can nuke yubico OTPs, where pre-built ones start with cccccc.

This is a simpler reimplementation of [my Discord bot with the same name](https://gitlab.com/a/c6).

---

Code is mostly based on [c6-discord](https://gitlab.com/a/c6) and [yurico-kupteraz](https://gitlab.com/a/yurico).

I host an instance on libera.chat. If you want my hosted version on your channel, ask a channel founder (`/cs info #examplechannel`) to PM me ("ave").
