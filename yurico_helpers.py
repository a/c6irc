import sys
from crccheck.crc import Crc16X25

modhex_base = "0123456789abcdef"
modhex_qwert = "cbdefghijklnrtuv"

re_public_id = r"^[cbdefghijklnrtuv]{0,12}$"
re_priv_id = r"^[0-9a-f]{12}$"
re_secret_key = r"^[0-9a-f]{32}$"
re_otp = r"^[cbdefghijklnrtuv]{32,48}$"
re_yubicloud_otp = r"^(cc|vv)[cbdefghijklnrtuv]{42}$"
re_yubicloud_otp_anywhere = r"((cc|vv)[cbdefghijklnrtuv']{42})"

yubicloud_prefixes = ["cc", "vv"]
yubicloud_servers = [
    "https://api.yubico.com",
    "https://api2.yubico.com",
    "https://api3.yubico.com",
    "https://api4.yubico.com",
    "https://api5.yubico.com",
]
yubicloud_reuse_responses = ["BAD_OTP", "REPLAYED_OTP"]
yubicloud_bad_responses = [
    "MISSING_PARAMETER",
    "NO_SUCH_CLIENT",
    "OPERATION_NOT_ALLOWED",
]


def hex_to_modhex(otp: str) -> str:
    """Convert hex str to modhex, based on code by linuxgemini"""
    hexconv = []

    for modhexletter in otp:
        modhex_index = modhex_base.index(modhexletter)
        hexconv.append(modhex_qwert[modhex_index])

    return "".join(hexconv)


def modhex_to_hex(otp: str) -> str:
    """Get modhex str to hex str, based on code by linuxgemini"""
    hexconv = []

    for modhexletter in otp:
        modhex_index = modhex_qwert.index(modhexletter)
        hexconv.append(modhex_base[modhex_index])

    return "".join(hexconv)


def reverse_endianness(data: bytes) -> bytes:
    data_reversed = bytearray(data)
    data_reversed.reverse()
    return bytes(data_reversed)


def calc_yubico_otp_checksum(data):
    checksum_sess = Crc16X25()
    checksum_sess.process(data[0:14])
    checksum_he = checksum_sess.finalbytes()
    checksum_le = reverse_endianness(checksum_he)
    return checksum_le


def le_bytes_to_int(data: bytes) -> int:
    return int.from_bytes(data, "little", signed=False)


def fancy_dict_print(data: dict):
    for entry, value in data.items():
        entry_printable = entry.replace("_", " ").title()
        print_bold(f"{entry_printable}: ", "")
        print(value)


def print_bold(data: str, end="\n", stderr=False):
    output_file = sys.stderr if stderr else sys.stdout
    print(f"\033[1m{data}\033[0m", end=end, file=output_file)


def get_serial(otp: str, force=False) -> int:
    """Get OTP from serial, based on code by linuxgemini"""
    if otp[:2] != "cc" and not force:
        return 0

    pub_id = otp[0:-32]

    return int(modhex_to_hex(pub_id), 16)
